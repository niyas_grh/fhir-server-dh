using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Microsoft.Health.Fhir.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GRH.DH.Fhir.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            // services.AddDevelopmentIdentityProvider(Configuration);

            Microsoft.Health.Fhir.Core.Registration.IFhirServerBuilder fhirServerBuilder = services.AddFhirServer(Configuration);

            fhirServerBuilder.AddSqlServer(Configuration);

            /*
            The execution of IHostedServices depends on the order they are added to the dependency injection container, so we
            need to ensure that the schema is initialized before the background workers are started.
            */
            fhirServerBuilder.AddBackgroundWorkers();

            if (string.Equals(Configuration["ASPNETCORE_FORWARDEDHEADERS_ENABLED"], "true", StringComparison.OrdinalIgnoreCase))
            {
                services.Configure<ForwardedHeadersOptions>(options =>
                {
                    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                        ForwardedHeaders.XForwardedProto;

                    // Only loopback proxies are allowed by default.
                    // Clear that restriction because forwarders are enabled by explicit
                    // configuration.
                    options.KnownNetworks.Clear();
                    options.KnownProxies.Clear();
                });
            }

            //if (bool.TryParse(Configuration["PrometheusMetrics:enabled"], out bool prometheusOn) && prometheusOn)
            //{
            //    services.AddPrometheusMetrics(Configuration);
            //}

            //AddApplicationInsightsTelemetry(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app)
        {
            if (string.Equals(Configuration["ASPNETCORE_FORWARDEDHEADERS_ENABLED"], "true", StringComparison.OrdinalIgnoreCase))
            {
                app.UseForwardedHeaders();
            }

            //app.UsePrometheusHttpMetrics();
            app.UseFhirServer();
            //app.UseDevelopmentIdentityProviderIfConfigured();
        }

        /// <summary>
        /// Adds ApplicationInsights for telemetry and logging.
        /// </summary>
        //private void AddApplicationInsightsTelemetry(IServiceCollection services)
        //{
        //    string instrumentationKey = Configuration["ApplicationInsights:InstrumentationKey"];

        //    if (!string.IsNullOrWhiteSpace(instrumentationKey))
        //    {
        //        services.AddApplicationInsightsTelemetry(instrumentationKey);
        //        services.AddSingleton<ITelemetryInitializer, CloudRoleNameTelemetryInitializer>();
        //        services.AddLogging(loggingBuilder => loggingBuilder.AddApplicationInsights(instrumentationKey));
        //    }
        //}
    }
}
