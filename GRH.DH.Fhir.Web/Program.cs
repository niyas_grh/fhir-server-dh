using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GRH.DH.Fhir.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(Path.GetDirectoryName(typeof(Program).Assembly.Location))
                .ConfigureAppConfiguration((hostContext, builder) =>
                {
                    //var builtConfig = builder.Build();

                    //var keyVaultEndpoint = builtConfig["KeyVault:Endpoint"];
                    //if (!string.IsNullOrEmpty(keyVaultEndpoint))
                    //{
                    //    var credential = new DefaultAzureCredential();
                    //    builder.AddAzureKeyVault(new System.Uri(keyVaultEndpoint), credential);
                    //}

                    //builder.AddDevelopmentAuthEnvironmentIfConfigured(builtConfig);
                })
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
